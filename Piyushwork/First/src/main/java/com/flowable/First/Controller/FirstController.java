package com.flowable.First.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.flowable.First.Pojo.UserPojo;
import com.flowable.First.Service.FirstService;

enum Mobile{
	
	APPLE,SAMSUNG,MI
}


@RestController
public class FirstController {
	
	@Autowired
	FirstService service;
	
	@Value("${app.title}")
	private String appTitle;
	
	

	
   public static final int RED=5;
   public void setColor(int x)
   {
	   System.out.println(x);
	   
	  
   }
   
   public void someMethod()
   {
	   setColor(5);
   }
   
   public static final int GREEN=5;
   
   
	
	@PostMapping("/submit")
	public void submit( @RequestBody UserPojo p)
	{
		System.out.println("-------------------"+appTitle);
		 System.out.println(Mobile.APPLE);
			service.post(p);
	}
	
	@GetMapping("/complete/{id}") 
	public void completeTask(@PathVariable String id) throws Exception
	{
		service.completeTask(id);
	}
	
	@GetMapping("/complete3/{id}")
	public void completeTask3(@PathVariable String id) throws Exception
	{
		service.completeTask3(id);
	}

}
