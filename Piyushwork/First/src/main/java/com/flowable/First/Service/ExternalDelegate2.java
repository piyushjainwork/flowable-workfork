package com.flowable.First.Service;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("ExternalDelegate2")
public class ExternalDelegate2 {

	@Autowired
	private RuntimeService runTimeService;

	public DelegateExecution saveMe(DelegateExecution execution) throws Exception  {
		System.out.println("Save Me");

		String processInstance = execution.getProcessInstanceId();
		System.out.println("this is Service task 2 ");
		int i=5;
	if(i>4)
		{
			throw new Exception("value is greater than 4 ");
		}
		
		runTimeService.setVariable(processInstance, "Save", "SaveMe");
		return execution;
		
	}
}
