package com.database.Fileupload1.Service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.database.Fileupload1.Exception.FileNotFoundException;
import com.database.Fileupload1.Exception.FileStorageException;
import com.database.Fileupload1.Repository.DatabaseFileRepository;
import com.database.Fileupload1.model.DatabaseFile;

public class DatabaseFileService {

	@Autowired
	DatabaseFileRepository dbFileRepository;

	public DatabaseFile storeFile(MultipartFile file) {
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! file name contains invalid path sequence " + fileName);

			}

			DatabaseFile dbFile = new DatabaseFile(fileName, file.getContentType(), file.getBytes());
			return dbFileRepository.save(dbFile);
		} catch (IOException ex) {
			throw new FileStorageException("Could not store file " + fileName + "Please Try again!", ex);
		}
	}
		public DatabaseFile getFile(String fileId) {
			return dbFileRepository.findById(fileId).orElseThrow(()-> new FileNotFoundException
					("File not found with id"+fileId));
		}
}
