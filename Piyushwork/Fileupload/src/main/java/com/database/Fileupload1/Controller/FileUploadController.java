package com.database.Fileupload1.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.database.Fileupload1.Payload.Response;
import com.database.Fileupload1.Service.DatabaseFileService;
import com.database.Fileupload1.model.DatabaseFile;

@RestController
public class FileUploadController {

	@Autowired
    private DatabaseFileService fileStorageService;
	
	@PostMapping("/uploadFile")
	public Response uploadFile(@RequestParam("file")MultipartFile file) {
		DatabaseFile fileName= fileStorageService.storeFile(file);
		
		
		String fileDownloadUri= ServletUriComponentsBuilder.fromCurrentContextPath().path("downloadFile/").path(fileName.getFileName()).toUriString();
		
		return new Response(fileName.getFileName(),fileDownloadUri,file.getContentType(),file.getSize());
	}
}
