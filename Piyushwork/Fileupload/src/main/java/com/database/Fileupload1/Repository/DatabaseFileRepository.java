package com.database.Fileupload1.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.database.Fileupload1.model.DatabaseFile;

@Repository
public interface DatabaseFileRepository  extends JpaRepository<DatabaseFile, String>{
	
	
	

}
