package com.example.demo;

import java.util.Map;

import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.flowable.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.springframework.stereotype.Service;



@Service
public class CallExternalSystemDelegate implements JavaDelegate {
	
	ProcessEngineConfiguration cfg = new StandaloneProcessEngineConfiguration()
	.setJdbcUrl("jdbc:h2:mem:flowable;DB_CLOSE_DELAY=-1").setJdbcUsername("sa").setJdbcPassword("")
	.setJdbcDriver("org.h2.Driver")
	.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);

	ProcessEngine processEngine = cfg.buildProcessEngine();
	
	
	RuntimeService runtimeService = processEngine.getRuntimeService();
	
	
	
	
	@Override
	public void execute(DelegateExecution execution) {
		
		
		
		
		String processInstanceId= execution.getProcessInstanceId();
		//System.out.println("Hello  ProcessInstance :"+ProcessInstanceId);

		runtimeService.setVariable(processInstanceId, "employee", "Piyush Jain");
		//System.out.println("the name of employee is : " + v.get("employee") );
		
		System.out.println(runtimeService.getVariableLocal(processInstanceId, "employee"));
		 System.out.println("Calling the external system for employee "
		 + execution.getVariable("employee"));
		 String date = "26/11/2019";
		 runtimeService.setVariableLocal(processInstanceId, "Date", date);
		 
		Map<String,Object> h1= runtimeService.getVariables(processInstanceId);
		
		System.out.println("The keys are :" +h1.keySet());
		
		System.out.println(h1.get("Date"));
		
	//	System.out.println(runtimeService.createProcessInstanceQuery());
		
		
		
		 
		 
		
		 
	}

}