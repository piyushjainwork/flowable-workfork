package com.kuliza.Assignments.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UsersPojo {
	
	private String name;
	
	private String phone;
	
	private int salary;
	
	private String age;
	
	
	List<HashMap<String,String>> address=new ArrayList<>();


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public int getSalary() {
		return salary;
	}


	public void setSalary(int salary) {
		this.salary = salary;
	}


	public String getAge() {
		return age;
	}


	public void setAge(String age) {
		this.age = age;
	}


	public List<HashMap<String, String>> getAddress() {
		return address;
	}


	public void setAddress(List<HashMap<String, String>> address) {
		this.address = address;
	}
	
	
	
	
	

}
