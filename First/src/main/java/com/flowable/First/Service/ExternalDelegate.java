package com.flowable.First.Service;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("ExternalDelegate")
public class ExternalDelegate  {
	
	@Autowired
	private RuntimeService runTimeService;
	
	public DelegateExecution saveCurrentDate(DelegateExecution execution) throws Exception {
		System.out.println("ABC");
		
		String processInstance = execution.getProcessInstanceId();
		int i=5;
		if(i>4)
			{
				throw new Exception("value is greater than 4 ");
			}
		System.out.println("THis is it");
		runTimeService.setVariable(processInstance, "temp", "temp1");
		return execution;
	}

}
