package com.flowable.First.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flowable.First.Pojo.UserPojo;

@Service
public class FirstService {

//	@Autowired
//	Deployment deploy;

	@Autowired
	RuntimeService runService;

	@Autowired
	TaskService taskService;

	public void post(UserPojo p) {
		Map<String, Object> variables = new HashMap<>();
		variables.put("name", p.getName());
		variables.put("phone", p.getPhone());
		ProcessInstance processInstance = runService.startProcessInstanceByKey("third", variables);

		System.out.println("Process instance" + processInstance.getId());

	}

	public void completeTask(String id) throws Exception  {
		System.out.println("Coming");
		
		List<Task> tasks = taskService.createTaskQuery().processInstanceId(id).active().list();
		if (tasks.size() >= 1) {
			System.out.println("inside");
			Task task = tasks.get(0);
			
			
			taskService.complete(task.getId());

			System.out.println("TaskComplete");

//			List<Task> tasks1 = taskService.createTaskQuery().processInstanceId(id).active().list();
//
//			if (tasks1.size() > 0) {
//				System.out.println("Again Inside");
//				Task t1 = tasks1.get(0);
//
//				taskService.complete(t1.getId());
			
		}

	}
	
	public void completeTask3(String id) throws Exception
	{
		List<Task> tasks1 = taskService.createTaskQuery().processInstanceId(id).active().list();

		if (tasks1.size() > 0) {
			//System.out.println("Again Inside");
			
//			int i=5;
//			if(i<=5)
//			{
//				throw new Exception( "value is less than expected" );
//				
//			}
			Task t1 = tasks1.get(0);

			taskService.complete(t1.getId());
		}
	}

}
