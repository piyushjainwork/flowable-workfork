package com.fileUpload.FileSystem.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fileUpload.FileSystem.Model.UserDetails;

public interface UserDetailsRepository extends JpaRepository<UserDetails, String> {

}
